## Introduction
This document describes how to incorporate a GKE cluster with GitLab's CICD.

The `.gitlab-ci.yml` contains two stages:
* `build`: the Docker image is build and pushed to the registry
* `deploy`: the deployment is applied in Kubernetes, pulling said image from the registry and starting the container.
This stage requires the Kubernetes cluster to authenticate with the Docker registry.

There are multiple configurations possible between GitLab and the Kubernetes cluster. These configurations differ in 
whether GitLab will or will not manage the cluster and as such in how namespaces are utilized in Kubernetes and in the 
way the Docker registry credentials are provided to Kubernetes.

* **Manually managed cluster**  
    This is applicable, when `GitLab-managed cluster` has **not** been checked in *Operations* > *Kubernetes* > 
    `<cluster name>` > *Kubernetes cluster details*.
    
    A single namespace is used by GitLab to deploy in. This namespace needs to be created manually beforehand. The 
    Docker registry credentials will be provided by manually defining a secret within this namespace. To following
    options are available to enforce a deployment in this specific namespace:
    - Specify the namespace name in *Operations* > *Kubernetes* > `<cluster name>` > *Kubernetes cluster details* >
     *Project namespace (optional, unique)*
    - Specify the namespace in `.gitlab-ci.yml` by using `kubectl apply -f deployment.yaml -n <namespace name>` in the
    `script`
    - Specify the namespace in `.gitlab-ci.yml` by adding
        ```
        environment:
            kubernetes:
                namespace: <namespace name>
        ``` 

* **GitLab managed cluster**  
    This is applicable, when `GitLab-managed cluster` has been checked in *Operations* > *Kubernetes* > 
    `<cluster name>` > *Kubernetes cluster details*.

    GitLab will create a namespace per project. In order to ensure the registry credentials are
    available in the `deploy` stage, they will need to be passed through GitLab's environment variables. See [[1]] on
    how to define such variables for the registry user and token (e.g. `REGISTRY_USER` and `REGISTRY_TOKEN`
    respectively). [[2]] shows how to these credentials.  
    Next, add the following snippet to `.gitlab-ci.yml` to register a secret in the project namespace before the main
    script is executed:
    ```
    before_script:
        - kubectl create secret docker-registry gitlab-docker-registry
            --docker-server="https://"$CI_REGISTRY
            --docker-username=$REGISTRY_USER
            --docker-password=$REGISTRY_TOKEN
            --save-config --dry-run -o yaml |
            kubectl apply -f -
    ```

**The remainder will assume the first configuration.**

## Steps 
 
1. **Configure the Kubernetes cluster**

    1. **Create namespace**  
        This step will create the `gitlab` namespace, used for our deployments.     
        ```
        kubectl apply -f kubernetes/namespace.yaml
        ```
  
    1. **Create a GitLab registry secret**  
        See [[2]] for steps on retrieving a personal access token. These credentials ensure
        access to the GitLab registry for the service account created in the next step.          
        ```
        kubectl create secret docker-registry gitlab-docker-registry \
        --namespace=gitlab \
        --docker-server=https://registry.gitlab.com \
        --docker-username=<docker_user> \
        --docker-password=<docker_password>
        ```
    1. **Create service account**
        ```
        kubectl apply -f kubernetes/service-account.yaml
        ```

1. **Add the GKE cluster to GitLab**
  
    The following steps will provide the information needed to add the GKE cluster to GitLab. See
    [[3]] for configuration steps within GitLab itself.  
    Make sure to add the cluster **on project level**, as on group level, `kubectl` will not be configured correctly in the runner.
     
    * API URL  
    `kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`
    * CA certificate
        1. Get `<secret name>` by listing the secrets through `kubectl get secrets -n gitlab`, and look for the one named
        `gitlab-service-account-token-xxxxx`.
        1. Get the certificate by running `kubectl get secret <secret name> -n gitlab -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`
    * Token  
    Get the token associated with the service account created in the previous step through
     `kubectl -n gitlab describe secret $(kubectl -n gitlab get secret | grep gitlab-service-account | awk '{print $1}')`    

## Troubleshooting
- `error: unable to recognize "STDIN": Get http://localhost:8080/api?timeout=32s: dial tcp [::1]:8080: connect: connection refused`  
`kubectl` has not been configured correctly in the runner.
  * Did you assign the Kubernetes cluster to a group instead of
directly to the project?
  * Did you specify an environment in your `.gitlab-ci.yml`?  
     ```
     environment:
       name: foo
     ```
- `error: unable to recognize [DEPLOYMENT YML]: Unauthorized`  
Does the Kubernetes namespace `<project>-<project id>-<environtment>` exist? GitLab creates this in the initial run. If
the namespace is delete from the cluster, GitLab might not be aware of this and will not to recreate. Try clearing the 
cluster cache in GitLab: `project` > *Operations* > *Kubernetes* > `<cluster>` > *Advanced settings* > 
**Clear cluster cache**.

## Useful links
- [GitLab CI documentation](https://docs.gitlab.com/ce/ci/yaml/README.html)
- [Using the same GKE cluster for multiple projects in Gitlab](https://medium.com/asl19-developers/connect-your-kubernetes-cluster-to-multiple-gitlab-projects-for-ci-cd-e922311172e3)


[1]: https://docs.gitlab.com/ee/ci/variables/#creating-a-custom-environment-variable
[2]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
[3]: https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#existing-gke-cluster

